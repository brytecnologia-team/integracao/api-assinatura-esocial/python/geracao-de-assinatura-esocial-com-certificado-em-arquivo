import requests
from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from base64 import b64encode, b64decode
from constant import AUTHORIZATION, FILE_PATH, CERT_PATH, CERT_PASSWORD, URL_FINALIZATION, URL_INITIALIZATION

file = open(FILE_PATH, 'rb').read()
HEADER = {'Authorization': AUTHORIZATION}

def token_verify():
    aux_authorization = HEADER['Authorization']
    if(aux_authorization == 'insert-a-valid-token'):
        print("É necessário inserir um token válido!")
        return False
    else:
        return True

def initialize_singature():

    print("================ Inicializando Assinatura XML ... ================")
    print("")

    # Passo 1: Carrega chave privada e certificado digital.

    original_documents = []
    original_documents.append(('originalDocuments[0][content]', file))

    p12 = crypto.load_pkcs12(open(CERT_PATH, 'rb').read(), CERT_PASSWORD)
    cert = p12.get_certificate()
   
    cert_base_64 = crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8').replace('-----BEGIN CERTIFICATE-----', '').replace('-----END CERTIFICATE-----', '').replace('\n', '')


    initialization_form = {
        'nonce': 1, 
        'signatureFormat': 'ENVELOPED',
        'hashAlgorithm': 'SHA256',
        'certificate': cert_base_64,
        'profile': 'BASIC',
        'returnType':'BASE64',
        'canonicalizerType':'INCLUSIVE',
        'generateSimplifiedXMLDSig':'true',
        'includeXPathEnveloped':'false',
        'originalDocuments[0][nonce]': '1'
    }

    # Passo 2: Inicializa a assinatura e produz os atributos a serem assinados

    response = requests.post(URL_INITIALIZATION, data = initialization_form, headers=HEADER, files=original_documents)
    if response.status_code == 200:

        data = response.json()
        print("Resposta JSON da Inicialização: ", data)
        print("")

        # Passo 3: Utilização da chave privada do certificado em disco para assinar os atributos da inicialização

        signed_attributes_content_decoded = b64decode(data['signedAttributes'][0]['content'])

        encrypted_message_digest_b64 = encrypt_initialized_signature(signed_attributes_content_decoded, p12.get_privatekey())


        # Passo 4: Finalização da assinatura e obtenção do artefato assinado

        finalize_signature(cert_base_64, data, encrypted_message_digest_b64)


    else:
        print(response.text)


def encrypt_initialized_signature(signed_attributes_content_decoded, private_key):

    key = crypto.dump_privatekey(crypto.FILETYPE_PEM, private_key).decode("utf-8")
    rsa_key = RSA.importKey(key)


    hash_object = SHA256.new(signed_attributes_content_decoded)
        
    signature = PKCS1_v1_5.new(rsa_key).sign(hash_object)

    return b64encode(signature)
        

def finalize_signature(cert_base_64, initialization_data, message_digest_cifrado_b64):

    print("================ Finalizando Assinatura XML ... ================")
    print("")

    form_finalization = {
        'nonce': 1, 
        'signatureFormat': 'ENVELOPED',
        'hashAlgorithm': 'SHA256',
        'certificate': cert_base_64,
        'profile': 'BASIC',
        'returnType':'BASE64',
        'canonicalizerType':'INCLUSIVE'
        }

    original_documents = []
    original_documents.append(('finalizations[0][content]', file))

    form_finalization['finalizations[0][nonce]'] = 1
    form_finalization['finalizations[0][initializedDocument]'] = initialization_data['initializedDocuments'][0]['content']
    form_finalization['finalizations[0][signatureValue]'] = message_digest_cifrado_b64

    response = requests.post(URL_FINALIZATION, data = form_finalization, headers=HEADER, files=original_documents)

    if response.status_code == 200:

        data = response.json()
        print("Resposta JSON da Finalização: ", data)
        print("")
        signature_content = data[0]
        signature_byte_array = b64decode(signature_content.encode("utf-8"))
        new_file = open("AssinaturaESocial.xml", 'wb')
        new_file.write(signature_byte_array)
        new_file.close()

        print("Assinatura gerada com sucesso e armazenada na pasta local como 'AssinaturaESocial.xml'")

    else:
        print(response.text)


if(token_verify()):
    initialize_singature()
