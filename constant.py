URL_INITIALIZATION = 'https://hub2.hom.bry.com.br/api/xml-signature-service/v2/signatures/initialize'
URL_FINALIZATION = 'https://hub2.hom.bry.com.br/api/xml-signature-service/v2/signatures/finalize'
#URL Para ambientes de produção
#URL_INITIALIZATION = 'https://hub2.bry.com.br/api/xml-signature-service/v2/signatures/initialize'
#URL_FINALIZATION = 'https://hub2.bry.com.br/api/xml-signature-service/v2/signatures/finalize'

AUTHORIZATION = 'insert-a-valid-token'

FILE_PATH = "./files-to-sign/eSocial.xml"
CERT_PATH = './caminho/para/o/certificado.p12'
CERT_PASSWORD = 'Senha Certificado'